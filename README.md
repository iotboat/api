# IOT Boat Remote API

This project contains the code of the RESTful web api for the remote dashboard of the iot boat project.
The iot boat project aims to bring features and advantages of modern digital hardware to old analogue boat-hardware.

The remote dashboard functions as a management suite for users of the "iot on-boat system".
Through the remote dashboard, on-boat systems can be registered and monitored. Although a handful of devices are supposed to be controllable, the primary focus is on real-time monitoring and long-term analysis.

**The project is in development.**

## Shortcuts
Name | URL | basic auth | credentials
-------|------------ | --- | -
dev server | https://api.simonamadeus.de | - | -
swagger reference | https://app.swaggerhub.com/apis-docs/szudemj/boat/1.0.0 | - | -
grafana monitoring | https://grafana.simonamadeus.de | code / getmein | peter / peter
ansible server setup | https://gitlab.com/iotboat/ansible_monitored_debian | - | -


### Overview diagram
![Overview](./overview_diagram.svg)

### Technologies in use
- Hosting -> Debian 10 cloud server
- Reverse proxy -> nginx
- Web backend framework -> [actix-web](https://actix.rs/)
- Database -> postgresql
- Database controller -> [sqlx](https://github.com/launchbadge/sqlx)
- Logging -> [tracing](https://github.com/tokio-rs/tracing)
- Auth -> cookie-auth with JWT
- Monitoring -> Grafana + Prometheus (node_exporter)
- Reference documentation -> Swagger

For more dependencies look into ./Cargo.toml.
# Development
## Prerequisites
- *NIX based operating system
- Install [Docker](https://docs.docker.com/get-docker/)
- Install [Rust](https://www.rust-lang.org/tools/install)

## Local development
### Preparation
1. Install the dev tools
    ```bash
    rustup component add clippy
    rustup component add rustfmt
    cargo install rusty-hooks
    cargo install --locked --version=0.5.1 sqlx-cli --no-default-features --features postgres offline
    ```
2. Start the postgres database (This will use docker in the background)
    ```bash
    ./scripts/init_db.sh
    ```
### Run tests

```bash
ulimit -n 10000 # once, on mac only
cargo test

# In case some tests fail, try again with
cargo test -- --test-threads=1
```

### Run the app
```bash
cargo check # check for compiler warnings
cargo run # run the app
cargo build # build the app
cargo build --release # build an optimised release
```

## Deployments
Branch | URL | Branch | Trigger
-|-|-|-
dev server | https://api.simonamadeus.de | master | on-change

## Project structure
The overall project structure alignes to the [rust conventions](https://doc.rust-lang.org/cargo/guide/project-layout.html).

Main entrypoint is ./src/main.rs. 
To get a quick overview over registered routes and handlers you can look into ./src/startup.rs.

Looking at integration tests in ./tests may serve as a subsitute for specific documentation.

## Current Features
- [x] user handling
- [x] session handling
- [x] handling of on-boat systems (boats)
- [ ] remote data storage for on-boat systems
- [ ] real-time data visualisation
- [ ] remote control of boat hardware