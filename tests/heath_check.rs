mod common;

#[actix_rt::test]
async fn it_health_check_succeeds() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();

    // When
    let response = client
        .get(&format!("{}/health_check", &test_app.address))
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert!(response.status().is_success());
    assert_eq!(response.content_length(), Some(0));
}
