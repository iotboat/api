mod common;

#[actix_rt::test]
async fn create_user_returns_a_201_for_valid_data() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let body = "name=Ama%20Deus&email=amadeus%40gmail.com&password=password";

    // When
    let response = client
        .post(&format!("{}/users", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .body(body)
        .send()
        .await
        .expect("failed to execute request");

    // Then
    let saved = sqlx::query!("SELECT email, name, password FROM users",)
        .fetch_one(&test_app.pg_pool)
        .await
        .expect("Failed to fetch saved users.");
    assert_eq!(201, response.status().as_u16());
    assert_eq!(saved.name, "Ama Deus");
    assert_eq!(saved.email, "amadeus@gmail.com");
    assert!(argon2::verify_encoded(&saved.password, b"password").unwrap());
}

#[actix_rt::test]
async fn create_user_returns_a_400_when_data_is_missing() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let test_cases = vec![
        ("name=ama%20deus&password=password", "missing the email"),
        (
            "email=amazeus%40gmail.com&password=password",
            "missing the name",
        ),
        (
            "name=ama%20zeus&email=nice_mail%40gmail.com",
            "missing the password",
        ),
        ("", "missing name, email and password"),
    ];

    for (invalid_body, error_message) in test_cases {
        // When
        let response = client
            .post(&format!("{}/users", &test_app.address))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(invalid_body)
            .send()
            .await
            .expect("failed to execute request");

        // Then
        assert_eq!(
            400,
            response.status().as_u16(),
            "The API did not fail with 400 Bad Request when the payload was {}.",
            error_message
        );
    }
}

#[actix_rt::test]
async fn create_user_returns_a_409_if_user_exists() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let body = "name=ama%20deus&email=amadeus%40gmail.com&password=password";
    let _ = client
        .post(&format!("{}/users", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .body(body)
        .send()
        .await
        .expect("failed to execute request");

    // When
    let response = client
        .post(&format!("{}/users", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .body(body)
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(409, response.status().as_u16());
}

#[actix_rt::test]
async fn get_user_data_returns_200_and_user_data_for_valid_request() {
    // Given
    let test_app = common::spawn_app().await;
    let test_user = common::TestUser {
        name: "amadeus@zeus.com".to_string(),
        email: "ama deus".to_string(),
        pass: "secretpass".to_string(),
    };
    let id = common::insert_test_user(&test_app, &test_user).await;
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&id, &secret, 4);
    let client = reqwest::Client::new();

    // When
    let response = client
        .get(&format!("{}/users", test_app.address))
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(200, response.status().as_u16());
    let body = &response.text().await.unwrap();
    let res_user_data: common::UserData =
        serde_json::from_str(body).expect("failed to parse response body");
    assert_eq!(id.to_string(), res_user_data.id);
    assert_eq!(test_user.email, res_user_data.email);
    assert_eq!(test_user.name, res_user_data.name);
}

#[actix_rt::test]
async fn get_user_data_returns_401_for_invalid_token() {
    // Given
    let test_app = common::spawn_app().await;
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let expired_jwt = common::create_expired_jwt(&uuid::Uuid::new_v4(), &secret);
    let client = reqwest::Client::new();

    // When
    let response = client
        .get(&format!("{}/users", test_app.address))
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &expired_jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(401, response.status().as_u16());
}

#[actix_rt::test]
async fn update_user_returns_200_and_changes_name_and_email_address() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let (email, name, pass) = (
        "beforeChange@zeus.com".to_string(),
        "ama deus".to_string(),
        "secretpass".to_string(),
    );
    let test_user = common::TestUser { name, email, pass };
    let new_user_id = common::insert_test_user(&test_app, &test_user).await;
    let test_cases = vec![
        ("name=new%20name%20neu", "changing the name"),
        ("email=afterChange%40test.com", "changing the email address"),
    ];
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&new_user_id, &secret, 4);

    for (request_body, description) in test_cases {
        // When
        let response = client
            .put(&format!("{}/users", &test_app.address))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .header(
                "Cookie",
                format!("auth_token={}; some=other_cookie; one more", &jwt),
            )
            .body(request_body)
            .send()
            .await
            .expect("failed to execute request");

        // Then
        assert_eq!(
            200,
            response.status().as_u16(),
            "The API did not return 200 when {}.",
            description
        );
    }

    // Then
    let saved = sqlx::query!("SELECT email, name FROM users",)
        .fetch_one(&test_app.pg_pool)
        .await
        .expect("Failed to fetch saved users.");
    assert_eq!("afterChange@test.com", saved.email);
    assert_eq!("new name neu", saved.name);
}

#[actix_rt::test]
async fn update_user_returns_401_for_invalid_token() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let request_body = "name=new%20name%20neu";
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = common::create_expired_jwt(&uuid::Uuid::new_v4(), &secret);

    // When
    let response = client
        .put(&format!("{}/users", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .body(request_body)
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(401, response.status().as_u16());
}

#[actix_rt::test]
async fn delete_user_returns_204_on_success() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let (email, name, pass) = (
        "amadeus@zeus.com".to_string(),
        "ama deus".to_string(),
        "secretpass".to_string(),
    );
    let test_user = common::TestUser { name, email, pass };
    let new_user_id = common::insert_test_user(&test_app, &test_user).await;
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&new_user_id, &secret, 4);

    // When
    let response = client
        .delete(&format!("{}/users", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(
        204,
        response.status().as_u16(),
        "The API did not return 204 when removing user.",
    );
    assert!(sqlx::query!(
        r#"
            SELECT *
            FROM users
            WHERE id = $1
            "#,
        new_user_id
    )
    .fetch_one(&test_app.pg_pool)
    .await
    .is_err());
}

#[actix_rt::test]
async fn delete_user_returns_401_for_invalid_token() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let (email, name, pass) = (
        "amadeus@zeus.com".to_string(),
        "ama deus".to_string(),
        "secretpass".to_string(),
    );
    let test_user = common::TestUser { name, email, pass };
    let new_user_id = common::insert_test_user(&test_app, &test_user).await;
    let before = sqlx::query!(
        r#"
        SELECT *
        FROM users
        WHERE id = $1
        "#,
        new_user_id
    )
    .fetch_one(&test_app.pg_pool)
    .await
    .expect("failed to select test user");
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = common::create_expired_jwt(&new_user_id, &secret);

    // When
    let response = client
        .delete(&format!("{}/users", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(
        401,
        response.status().as_u16(),
        "The API did not return 401 when using expired token.",
    );
    let after = sqlx::query!(
        r#"
            SELECT *
            FROM users
            WHERE id = $1
            "#,
        new_user_id
    )
    .fetch_one(&test_app.pg_pool)
    .await
    .expect("failed to select test user");
    assert_eq!(before.id, after.id);
}

#[actix_rt::test]
async fn delete_user_expires_cookie() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let (email, name, pass) = (
        "amadeus@zeus.com".to_string(),
        "ama deus".to_string(),
        "secretpass".to_string(),
    );
    let test_user = common::TestUser { name, email, pass };
    let new_user_id = common::insert_test_user(&test_app, &test_user).await;
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&new_user_id, &secret, 4);

    // When
    let response = client
        .delete(&format!("{}/users", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    let value = response
        .headers()
        .get("Set-Cookie")
        .expect("failed to find \"Set-Cookie\"");
    assert_eq!(204, response.status().as_u16());
    assert!(value.to_str().unwrap().contains("expires"));
}
