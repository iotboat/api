use sqlx::Row;
use uuid::Uuid;

mod common;

#[actix_rt::test]
async fn create_boat_returns_a_201_for_valid_data() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let test_user = common::TestUser {
        name: "amadeus@zeus.com".to_string(),
        email: "ama deus".to_string(),
        pass: "secretpass".to_string(),
    };
    let boat_id1 = "firstBoatId";
    let boat_name1 = "fancyName";
    let request_body1 = format!(
        "authentication_id={}&name={}&password=thisissecret",
        boat_id1, boat_name1
    );
    let boat_id2 = "secondboatid";
    let boat_name2 = "newName";
    let request_body2 = format!(
        "authentication_id={}&name={}&password=thisissecret",
        boat_id2, boat_name2
    );
    let user_id = common::insert_test_user(&test_app, &test_user).await;
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&user_id, &secret, 4);

    // When
    let response1 = client
        .post(&format!("{}/boats", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .body(request_body1)
        .send()
        .await
        .expect("failed to execute first request");
    let response2 = client
        .post(&format!("{}/boats", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .body(request_body2)
        .send()
        .await
        .expect("failed to execute second request");

    // Then
    let saved =
        sqlx::query("SELECT authentication_id, name, user_id FROM boats WHERE user_id = $1")
            .bind(user_id)
            .fetch_all(&test_app.pg_pool)
            .await
            .expect("failed to execute query");
    let id1: &str = saved[0].get("authentication_id");
    let name1: &str = saved[0].get("name");
    let id2: &str = saved[1].get("authentication_id");
    let name2: &str = saved[1].get("name");
    assert_eq!(201, response1.status().as_u16());
    assert_eq!(201, response2.status().as_u16());
    assert_eq!(boat_id1, id1);
    assert_eq!(boat_name1, name1);
    assert_eq!(boat_id2, id2);
    assert_eq!(boat_name2, name2);
}

#[actix_rt::test]
async fn create_boat_returns_a_400_when_data_is_missing() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let test_user = common::TestUser {
        name: "ama deus".to_string(),
        email: "amadeus@zeus.com".to_string(),
        pass: "verysecret".to_string(),
    };
    let test_cases = vec![
        (
            "authentication_id=&name=FancyName&password=thisissecret",
            "missing value for id",
        ),
        (
            "authentication_id=charlie&name=&password=thisissecret",
            "missing value for name",
        ),
        ("name=Thomas&password=thisissecret", "missing id field"),
        (
            "authentication_id=EchoBravo&password=thisissecret",
            "missing name field",
        ),
        (
            "authentication_id=Thomas&name=myname&password=",
            "missing value for password",
        ),
        (
            "authentication_id=Thomas&name=myname",
            "missing password field",
        ),
        ("", "empty body string"),
    ];
    let user_id = common::insert_test_user(&test_app, &test_user).await;
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&user_id, &secret, 4);

    for (invalid_body, error_message) in test_cases {
        // When
        let response = client
            .post(&format!("{}/boats", &test_app.address))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .header(
                "Cookie",
                format!("auth_token={}; some=other_cookie; one more", &jwt),
            )
            .body(invalid_body)
            .send()
            .await
            .expect("failed to execute request");

        // Then
        assert_eq!(
            400,
            response.status().as_u16(),
            "The API did not fail with 400 Bad Request when the payload was {}.",
            error_message
        );
    }
}

#[actix_rt::test]
async fn create_boat_returns_401_for_invalid_or_no_jwt() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let body = "authentication_id=CalmariCheck&name=my fancy name";
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let invalid_jwt = common::create_expired_jwt(&Uuid::new_v4(), &secret);

    // When
    let invalid_jwt_response = client
        .post(&format!("{}/boats", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &invalid_jwt),
        )
        .body(body)
        .send()
        .await
        .expect("failed to execute request");
    let no_jwt_response = client
        .post(&format!("{}/boats", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .body(body)
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(401, invalid_jwt_response.status().as_u16());
    assert_eq!(401, no_jwt_response.status().as_u16());
}

#[actix_rt::test]
async fn create_boat_returns_a_409_if_id_exists() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let test_user = common::TestUser {
        name: "ama deus".to_string(),
        email: "amadeus@zeus.com".to_string(),
        pass: "verysecret".to_string(),
    };
    let body1 = "authentication_id=Contest&name=my fancy name&password=thisissecret";
    let body2 = "authentication_id=Contest&name=my different fancy name&password=thisissecret";
    let user_id = common::insert_test_user(&test_app, &test_user).await;
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&user_id, &secret, 4);
    let _ = client
        .post(&format!("{}/boats", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .body(body1)
        .send()
        .await
        .expect("failed to execute request");

    // When
    let response = client
        .post(&format!("{}/boats", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .body(body2)
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(409, response.status().as_u16());
}

#[actix_rt::test]
async fn create_boat_returns_a_409_if_name_exists() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let test_user = common::TestUser {
        name: "ama deus".to_string(),
        email: "amadeus@zeus.com".to_string(),
        pass: "verysecret".to_string(),
    };
    let body1 = "authentication_id=Contest&name=my fancy name&password=thisissecret";
    let body2 = "authentication_id=newContest&name=my fancy name&password=thisissecret";
    let user_id = common::insert_test_user(&test_app, &test_user).await;
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&user_id, &secret, 4);
    let _ = client
        .post(&format!("{}/boats", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .body(body1)
        .send()
        .await
        .expect("failed to execute request");

    // When
    let response = client
        .post(&format!("{}/boats", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .body(body2)
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(409, response.status().as_u16());
}

#[actix_rt::test]
async fn get_boats_returns_200_and_list_of_boats() {
    // Given
    #[derive(serde::Deserialize)]
    struct BoatInfo {
        id: String,
        authentication_id: String,
        name: String,
        user_id: String,
    }
    let test_app = common::spawn_app().await;
    let http_client = reqwest::Client::new();
    let test_user = common::TestUser {
        name: "ama deus".to_string(),
        email: "amadeus@zeus.com".to_string(),
        pass: "verysecret".to_string(),
    };
    let user_id = common::insert_test_user(&test_app, &test_user).await;
    let test_boat1 = common::TestBoat {
        auth_id: "abc123".to_string(),
        name: "MyFirstBoat".to_string(),
        password: "secret".to_string(),
        user_id,
    };
    let test_boat2 = common::TestBoat {
        auth_id: "321cba".to_string(),
        name: "MySecondBoat".to_string(),
        password: "secret".to_string(),
        user_id,
    };
    let boat_id1 = common::insert_test_boat(&test_app.pg_pool, &test_boat1, &user_id).await;
    let boat_id2 = common::insert_test_boat(&test_app.pg_pool, &test_boat2, &user_id).await;
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&user_id, &secret, 4);

    // When
    let response = http_client
        .get(&format!("{}/boats", &test_app.address))
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(200, response.status().as_u16());
    let raw_data = &response.text().await.expect("failed to find response body");
    let data: Vec<BoatInfo> =
        serde_json::from_str(&raw_data).expect("failed to parse json body to boat info");
    assert_eq!(boat_id1.to_string(), data[0].id);
    assert_eq!(test_boat1.auth_id, data[0].authentication_id);
    assert_eq!(test_boat1.name, data[0].name);
    assert_eq!(user_id.to_string(), data[0].user_id);
    assert_eq!(boat_id2.to_string(), data[1].id);
    assert_eq!(test_boat2.auth_id, data[1].authentication_id);
    assert_eq!(test_boat2.name, data[1].name);
    assert_eq!(user_id.to_string(), data[1].user_id);
}

#[actix_rt::test]
async fn get_boats_returns_401_for_invalid_token() {
    // Given
    #[derive(serde::Deserialize, Debug)]
    struct BoatInfo {
        id: String,
        authentication_id: String,
        name: String,
        user_id: String,
    }
    let test_app = common::spawn_app().await;
    let http_client = reqwest::Client::new();
    let test_user = common::TestUser {
        name: "ama deus".to_string(),
        email: "amadeus@zeus.com".to_string(),
        pass: "verysecret".to_string(),
    };
    let user_id = common::insert_test_user(&test_app, &test_user).await;

    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let expired_jwt = common::create_expired_jwt(&user_id, &secret);

    // When
    let response = http_client
        .get(&format!("{}/boats", &test_app.address))
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &expired_jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(401, response.status().as_u16());
}

#[actix_rt::test]
async fn delete_boat_returns_204_for_existing_resource() {
    // Given
    let test_app = common::spawn_app().await;
    let http_client = reqwest::Client::new();
    let test_user = common::TestUser {
        name: "ama deus".to_string(),
        email: "amadeus@zeus.com".to_string(),
        pass: "verysecret".to_string(),
    };
    let user_id = common::insert_test_user(&test_app, &test_user).await;
    let test_boat1 = common::TestBoat {
        auth_id: "abc123".to_string(),
        name: "MyFirstBoat".to_string(),
        password: "secret".to_string(),
        user_id,
    };
    let test_boat2 = common::TestBoat {
        auth_id: "321cba".to_string(),
        name: "MySecondBoat".to_string(),
        password: "secret".to_string(),
        user_id,
    };
    let _boat_id1 = common::insert_test_boat(&test_app.pg_pool, &test_boat1, &user_id).await;
    let boat_id2 = common::insert_test_boat(&test_app.pg_pool, &test_boat2, &user_id).await;
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&user_id, &secret, 4);

    // When
    let response = http_client
        .delete(&format!("{}/boats/{}", &test_app.address, &boat_id2))
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(204, response.status().as_u16());
    let boat2 = sqlx::query("SELECT * FROM boats WHERE id = $1")
        .bind(&boat_id2)
        .fetch_optional(&test_app.pg_pool)
        .await
        .expect("Failed to fetch saved users.");
    assert!(boat2.is_none());
}

#[actix_rt::test]
async fn delete_boat_returns_404_for_invalid_id_format() {
    // Given
    let test_app = common::spawn_app().await;
    let http_client = reqwest::Client::new();
    let test_user = common::TestUser {
        name: "ama deus".to_string(),
        email: "amadeus@zeus.com".to_string(),
        pass: "verysecret".to_string(),
    };
    let user_id = common::insert_test_user(&test_app, &test_user).await;
    let test_boat1 = common::TestBoat {
        auth_id: "abc123".to_string(),
        name: "MyFirstBoat".to_string(),
        password: "secret".to_string(),
        user_id,
    };
    let test_boat2 = common::TestBoat {
        auth_id: "321cba".to_string(),
        name: "MySecondBoat".to_string(),
        password: "secret".to_string(),
        user_id,
    };
    let _boat_id1 = common::insert_test_boat(&test_app.pg_pool, &test_boat1, &user_id).await;
    let boat_id2 = common::insert_test_boat(&test_app.pg_pool, &test_boat2, &user_id).await;
    let invalid_id = "slkdfj253_klsjfd54_3334624";
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&user_id, &secret, 4);

    // When
    let response = http_client
        .delete(&format!("{}/boats/{}", &test_app.address, invalid_id))
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(404, response.status().as_u16());
    let boat2 = sqlx::query("SELECT * FROM boats WHERE id = $1")
        .bind(&boat_id2)
        .fetch_optional(&test_app.pg_pool)
        .await
        .expect("Failed to fetch saved users.");
    assert!(boat2.is_some());
}

#[actix_rt::test]
async fn delete_boat_returns_404_if_resource_does_not_exist() {
    // Given
    let test_app = common::spawn_app().await;
    let http_client = reqwest::Client::new();
    let test_user = common::TestUser {
        name: "ama deus".to_string(),
        email: "amadeus@zeus.com".to_string(),
        pass: "verysecret".to_string(),
    };
    let user_id = common::insert_test_user(&test_app, &test_user).await;
    let fake_boat_id = Uuid::new_v4();
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&user_id, &secret, 4);

    // When
    let response = http_client
        .delete(&format!("{}/boats/{}", &test_app.address, &fake_boat_id))
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(404, response.status().as_u16());
}

#[actix_rt::test]
async fn delete_boat_returns_401_for_invalid_token() {
    // Given
    let test_app = common::spawn_app().await;
    let http_client = reqwest::Client::new();
    let test_user = common::TestUser {
        name: "ama deus".to_string(),
        email: "amadeus@zeus.com".to_string(),
        pass: "verysecret".to_string(),
    };
    let user_id = common::insert_test_user(&test_app, &test_user).await;
    let test_boat1 = common::TestBoat {
        auth_id: "abc123".to_string(),
        name: "MyFirstBoat".to_string(),
        password: "secret".to_string(),
        user_id,
    };
    let boat_id = common::insert_test_boat(&test_app.pg_pool, &test_boat1, &user_id).await;
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let expired_jwt = common::create_expired_jwt(&user_id, &secret);

    // When
    let response = http_client
        .delete(&format!("{}/boats/{}", &test_app.address, &boat_id))
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &expired_jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(401, response.status().as_u16());
}
