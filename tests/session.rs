mod common;

#[actix_rt::test]
async fn starting_a_session_returns_200_and_sets_cookie_for_valid_request() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let test_user = common::TestUser {
        email: "amadeus@zeus.com".to_string(),
        name: "ama deus".to_string(),
        pass: "topsecret".to_string(),
    };
    let request_body = "email=amadeus%40zeus.com&password=topsecret";
    common::insert_test_user(&test_app, &test_user).await;

    // When
    let response = client
        .post(&format!("{}/session", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .body(request_body)
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(200, response.status().as_u16());
    assert!(response.headers().contains_key("Set-Cookie"));
}

#[actix_rt::test]
async fn session_token_is_secured_with_flags() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let test_user = common::TestUser {
        email: "amadeus@zeus.com".to_string(),
        name: "ama deus".to_string(),
        pass: "topsecret".to_string(),
    };
    let request_body = "email=amadeus%40zeus.com&password=topsecret";
    common::insert_test_user(&test_app, &test_user).await;

    // When
    let response = client
        .post(&format!("{}/session", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .body(request_body)
        .send()
        .await
        .expect("failed to execute request");

    // Then
    let cookie = response
        .headers()
        .get("Set-Cookie")
        .expect("failed to get value for \"Set-Cookie\"");
    let test_parameters = vec!["HttpOnly", "SameSite=Strict"];
    for parameter in test_parameters {
        assert!(
            cookie.to_str().unwrap().contains(parameter),
            "Set-Cookie is missing attribute \"{}\"",
            parameter
        );
    }
}

#[actix_rt::test]
async fn start_session_returns_400_for_invalid_user_credentials() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let test_user = common::TestUser {
        email: "amadeus@zeus.com".to_string(),
        name: "ama deus".to_string(),
        pass: "topsecret".to_string(),
    };
    let wrong_mail_body = "email=wrongmail%40zeus.com&password=topsecret";
    let wrong_pw_body = "email=amadeus%40zeus.com&password=wrongpassword";
    common::insert_test_user(&test_app, &test_user).await;

    // When
    let wrong_mail_response = client
        .post(&format!("{}/session", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .body(wrong_mail_body)
        .send()
        .await
        .expect("failed to execute request");
    let wrong_pw_response = client
        .post(&format!("{}/session", &test_app.address))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .body(wrong_pw_body)
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(400, wrong_mail_response.status().as_u16());
    assert_eq!(400, wrong_pw_response.status().as_u16());
}

#[actix_rt::test]
async fn validate_session_returns_200_for_valid_token() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&uuid::Uuid::new_v4(), &secret, 4);

    // When
    let response = client
        .get(&format!("{}/session", test_app.address))
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(200, response.status().as_u16());
}

#[actix_rt::test]
async fn validate_session_returns_401_for_invalid_token() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();

    // When
    let response = client
        .get(&format!("{}/session", test_app.address))
        .header(
            "Cookie",
            "auth_token=skfhdskdhk2342kjhkj2; some=other_cookie; one more",
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    assert_eq!(401, response.status().as_u16());
}

#[actix_rt::test]
async fn end_session_returns_204_and_expires_valid_cookie() {
    // Given
    let test_app = common::spawn_app().await;
    let client = reqwest::Client::new();
    let secret = std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
    let jwt = iotboat_api::auth::create_jwt(&uuid::Uuid::new_v4(), &secret, 4);

    // When
    let response = client
        .delete(&format!("{}/session", test_app.address))
        .header(
            "Cookie",
            format!("auth_token={}; some=other_cookie; one more", &jwt),
        )
        .send()
        .await
        .expect("failed to execute request");

    // Then
    let set_cookie_value = response
        .headers()
        .get("Set-Cookie")
        .expect("failed to find \"Set-Cookie\"");
    assert_eq!(204, response.status().as_u16());
    assert!(set_cookie_value.to_str().unwrap().contains("expires"));
}
