use chrono::Utc;
use iotboat_api::auth::{create_secret, Claims};
use iotboat_api::configuration::{get_configuration, DatabaseSettings};
use iotboat_api::startup::run;
use iotboat_api::telemetry::{get_subscriber, init_subscriber};
use jsonwebtoken::{encode, EncodingKey, Header};
use rand::{thread_rng, Rng};
use serde::{Deserialize, Serialize};
use sqlx::{Executor, PgPool};
use std::net::TcpListener;
use std::time::{SystemTime, UNIX_EPOCH};
use uuid::Uuid;

pub struct TestApp {
    pub address: String,
    pub pg_pool: sqlx::PgPool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserData {
    pub id: String,
    pub name: String,
    pub email: String,
}

pub struct TestUser {
    pub name: String,
    pub email: String,
    pub pass: String,
}

pub struct TestBoat {
    pub auth_id: String,
    pub name: String,
    pub password: String,
    pub user_id: uuid::Uuid,
}

// lazy_static allows to treat heap allocated types as static variables
lazy_static::lazy_static! {
    static ref TRACING: () = {
        let filter = if std::env::var("TEST_LOG").is_ok() { "debug" } else { "" };
        let subscriber = get_subscriber("iotboat_api".into(), filter.into());
        init_subscriber(subscriber);
    };
}

pub async fn spawn_app() -> TestApp {
    std::env::set_var("JWT_SECRET", create_secret(16));
    lazy_static::initialize(&TRACING);
    let mut configuration = get_configuration().expect("Failed to get configuration.");
    configuration.database.database_name = uuid::Uuid::new_v4().to_string();
    let pg_pool = configure_database(&configuration.database).await;
    let listener = TcpListener::bind("127.0.0.1:0").expect("Failed to bind random port");
    let port = listener.local_addr().unwrap().port();
    let address = format!("http://127.0.0.1:{}/v1", port);
    let server = run(listener, pg_pool.clone()).expect("Failed to bind address");
    let _ = tokio::spawn(server);
    TestApp { address, pg_pool }
}

async fn configure_database(config: &DatabaseSettings) -> PgPool {
    let connection = PgPool::connect(&config.connection_string_without_db())
        .await
        .expect("Failed to connect to postgres.");
    connection
        .execute(&*format!(r#"CREATE DATABASE "{}";"#, config.database_name))
        .await
        .expect("Failed to create database.");
    let connection_pool = PgPool::connect(&config.connection_string())
        .await
        .expect("Failed to connect to Postgres.");
    sqlx::migrate!("./migrations")
        .run(&connection_pool)
        .await
        .expect("Failed to migrate the database.");

    connection_pool
}

#[allow(dead_code)]
pub async fn insert_test_user(test_app: &TestApp, test_user: &TestUser) -> Uuid {
    sqlx::query!(
        r#"
        INSERT INTO users (id, email, name, password, registered_at)
        VALUES ($1, $2, $3, $4, $5)
        ON CONFLICT DO NOTHING
        RETURNING id
        "#,
        Uuid::new_v4(),
        &test_user.email,
        &test_user.name,
        hash_password(&test_user.pass),
        Utc::now()
    )
    .fetch_optional(&test_app.pg_pool)
    .await
    .unwrap()
    .expect("failed to execute query")
    .id
}

fn hash_password(password: &str) -> String {
    let config = argon2::Config::default();
    let mut salt = [0u8; 16]; // 16 byte, as 128 bit is the recommended salt length for argon2
    thread_rng()
        .try_fill(&mut salt[..])
        .expect("Failed to generate random salt.");

    argon2::hash_encoded(password.as_bytes(), &salt, &config).expect("Failed to encode password.")
}

#[allow(dead_code)]
pub fn create_expired_jwt(user_id: &Uuid, secret: &str) -> String {
    let exp = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs()
        - 60;
    let claims = Claims {
        exp,
        user_id: user_id.to_string(),
    };

    encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(secret.as_ref()),
    )
    .expect("failed to encode jwt")
}

#[allow(dead_code)]
pub async fn insert_test_boat(pool: &PgPool, boat: &TestBoat, user_id: &Uuid) -> Uuid {
    sqlx::query!(
        r#"
        INSERT INTO boats (id, authentication_id, name, password, user_id, created_at)
        VALUES ($1, $2, $3, $4, $5, $6)
        ON CONFLICT DO NOTHING
        RETURNING id
        "#,
        Uuid::new_v4(),
        &boat.auth_id,
        &boat.name,
        hash_password(&boat.password),
        user_id,
        Utc::now()
    )
    .fetch_optional(pool)
    .await
    .unwrap()
    .expect("failed to execute query")
    .id
}
