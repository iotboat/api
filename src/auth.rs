use actix_web::error::ErrorUnauthorized;
use actix_web::{dev, Error, FromRequest, HttpRequest};
use argon2::{self, Config};
use futures::future::{err, ok, Ready};
use jsonwebtoken::{decode, encode, Algorithm, DecodingKey, EncodingKey, Header, Validation};
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use serde::{Deserialize, Serialize};
use std::time::{SystemTime, UNIX_EPOCH};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    pub exp: u64,
    pub user_id: String,
}

impl FromRequest for Claims {
    type Error = Error;
    type Future = Ready<Result<Self, Self::Error>>;
    type Config = ();

    fn from_request(req: &HttpRequest, _payload: &mut dev::Payload) -> Self::Future {
        let raw_cookie_option = req.headers().get("Cookie");

        let raw_cookie = match raw_cookie_option {
            None => return err(ErrorUnauthorized("failed to read cookie")),
            Some(header_value) => header_value.to_str().expect("failed to make cookie str"),
        };
        let cookie = raw_cookie.split(';').find(|i| i.contains("auth_token"));

        if cookie.is_none() {
            return err(ErrorUnauthorized("failed to find cookie"));
        }

        let jwt = cookie.unwrap().split('=').nth(1).unwrap().to_string();
        let jwt_secret =
            std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");

        let claims: Claims = if let Ok(token_data) = decode::<Claims>(
            &jwt,
            &DecodingKey::from_secret(jwt_secret.as_ref()),
            &Validation::new(Algorithm::HS256),
        ) {
            token_data.claims
        } else {
            return err(ErrorUnauthorized("token invalid"));
        };

        ok(claims)
    }
}

pub fn create_secret(length: u8) -> String {
    let mut rng = thread_rng();
    std::iter::repeat(())
        .map(|()| rng.sample(Alphanumeric))
        .map(char::from)
        .take(length.into())
        .collect::<String>()
}

pub fn create_jwt(user_id: &Uuid, secret: &str, expires_in_hours: u8) -> String {
    let user_id = user_id.to_string();
    let exp = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs()
        + 60 * 60 * expires_in_hours as u64;
    let claims = Claims { exp, user_id };

    encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(secret.as_ref()),
    )
    .expect("failed to encode jwt")
}

pub fn hash_password(password: &str) -> String {
    let config = Config::default();
    let mut salt = [0u8; 16]; // 16 byte, as 128 bit is the recommended salt length for argon2
    thread_rng()
        .try_fill(&mut salt[..])
        .expect("Failed to generate random salt.");
    argon2::hash_encoded(password.as_bytes(), &salt, &config).expect("Failed to encode password.")
}

#[cfg(test)]
mod tests {
    use super::*;

    use jsonwebtoken::{decode, Algorithm, DecodingKey, Validation};
    #[test]
    fn created_jwt_is_valid_after_creation() {
        // Given
        let user_id = Uuid::new_v4();
        let secret = "verymystical";
        let jwt = create_jwt(&user_id, secret, 4);

        // When
        let result = decode::<Claims>(
            &jwt,
            &DecodingKey::from_secret(secret.as_ref()),
            &Validation::new(Algorithm::HS256),
        );

        // Then
        assert!(result.is_ok())
    }

    #[test]
    fn created_secret_has_expected_length() {
        // Given
        let expected_length = 7;

        // When
        let length_of_secret = create_secret(expected_length).len() as u8;

        // Then
        assert_eq!(expected_length, length_of_secret);
    }
}
