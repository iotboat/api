use crate::auth::{create_jwt, Claims};
use actix_web::{web, HttpResponse};
use chrono::SubsecRound;
use serde::Deserialize;
use sqlx::PgPool;
use std::ops::Sub;
use tracing::instrument;

#[derive(Debug, Deserialize)]
pub struct LoginForm {
    email: String,
    password: String,
}

#[instrument(name = "Validate a user session")]
pub async fn validate_session(_claims: Claims) -> Result<HttpResponse, HttpResponse> {
    Ok(HttpResponse::Ok().finish())
}

#[instrument(
  name = "Start session for a user",
  skip(form, pool),
  fields(
      email = %form.email,
  )
)]
pub async fn start_session(
    form: web::Form<LoginForm>,
    pool: web::Data<PgPool>,
) -> Result<HttpResponse, HttpResponse> {
    let result = sqlx::query!(
        r#"SELECT id, password FROM users WHERE email = $1"#,
        &form.email
    )
    .fetch_optional(&**pool)
    .await
    .map_err(|err| {
        tracing::error!("Failed to execute query: {:?}", err);
        err
    })
    .unwrap();

    if result.is_none() {
        return Ok(HttpResponse::BadRequest().finish());
    }
    let result = result.unwrap();
    let stored_hash = result.password;
    let user_id = result.id;
    if argon2::verify_encoded(&stored_hash, form.password.as_bytes())
        .expect("failed to match password")
    {
        let cookie_name = "auth_token";
        let jwt_secret =
            std::env::var("JWT_SECRET").expect("failed to read jwt_secret from environment");
        let jwt = create_jwt(&user_id, &jwt_secret, 4);
        return Ok(HttpResponse::Ok()
            .insert_header((
                "Set-Cookie",
                format!("{}={}; HttpOnly; SameSite=Strict", cookie_name, jwt),
            ))
            .finish());
    }

    Ok(HttpResponse::BadRequest().finish())
}

#[instrument(name = "End a user session")]
pub async fn end_session() -> Result<HttpResponse, HttpResponse> {
    let yesterday = chrono::Utc::now()
        .sub(chrono::Duration::days(1))
        .trunc_subsecs(0)
        .to_rfc2822();
    let cookie_name = "auth_token";
    Ok(HttpResponse::NoContent()
        .insert_header((
            "Set-Cookie",
            format!(
                "{}=\"\"; HttpOnly; SameSite=Strict; expires={}",
                cookie_name, yesterday
            ),
        ))
        .finish())
}
