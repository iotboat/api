use crate::auth::{hash_password, Claims};
use actix_web::{web, HttpResponse};
use chrono::SubsecRound;
use chrono::Utc;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;
use std::ops::Sub;
use tracing::instrument;
use uuid::Uuid;
use validator::Validate;

#[derive(Debug, Serialize, Deserialize)]
pub struct UserData {
    pub id: String,
    pub name: String,
    pub email: String,
}

#[derive(Deserialize, Validate)]
pub struct SignUpForm {
    name: String,
    #[validate(email)]
    email: String,
    password: String,
}

#[derive(Deserialize, Debug)]
pub struct UpdateUserForm {
    name: Option<String>,
    email: Option<String>,
}

#[instrument(name = "Fetching data of session user", skip(pool))]
pub async fn return_user_info(
    claims: Claims,
    pool: web::Data<PgPool>,
) -> Result<HttpResponse, HttpResponse> {
    let user_id: Uuid = Uuid::parse_str(&claims.user_id).expect("failed to parse uuid");
    let (email, name) = if let Some(record) =
        sqlx::query!(r#"SELECT email, name FROM users WHERE id = $1"#, &user_id)
            .fetch_optional(&**pool)
            .await
            .map_err(|err| {
                tracing::error!("Failed to execute query: {:?}", err);
                err
            })
            .unwrap()
    {
        (record.email, record.name)
    } else {
        return Ok(HttpResponse::Unauthorized().finish());
    };

    let response_body = serde_json::to_string(&UserData {
        id: claims.user_id.to_string(),
        email,
        name,
    })
    .expect("failed to build response");

    Ok(HttpResponse::Ok().body(response_body))
}

#[instrument(
    name = "Adding a new user",
    skip(form, pool),
    fields(
        email = %form.email,
        name = %form.name
    )
)]
pub async fn create_user(
    form: web::Form<SignUpForm>,
    pool: web::Data<PgPool>,
) -> Result<HttpResponse, HttpResponse> {
    if let Err(err) = form.validate() {
        tracing::error!("Failed to validate email format: {:?}", err);
        return Ok(HttpResponse::Conflict().finish());
    }
    let password = hash_password(&form.password);
    let user_data = SignUpForm {
        password,
        ..form.into_inner()
    };
    if insert_user(&pool, &user_data)
        .await
        .map_err(|_| HttpResponse::InternalServerError().finish())?
    {
        Ok(HttpResponse::Created().finish())
    } else {
        Ok(HttpResponse::Conflict().finish())
    }
}

#[instrument(name = "Updating user data", skip(form, pool))]
pub async fn update_user(
    claims: Claims,
    form: web::Form<UpdateUserForm>,
    pool: web::Data<PgPool>,
) -> Result<HttpResponse, HttpResponse> {
    let user_id: Uuid = Uuid::parse_str(&claims.user_id).expect("failed to parse uuid");
    if let Some(value) = &form.email {
        let _result = sqlx::query!(
            r#"
            UPDATE users
            SET email = ($1)
            WHERE id = ($2)
            "#,
            value,
            user_id
        )
        .fetch_optional(&**pool)
        .await
        .map_err(|err| {
            tracing::error!("Failed to execute query: {:?}", err);
            err
        })
        .expect("failed to update user email");
    }
    if let Some(value) = &form.name {
        let _result = sqlx::query!(
            r#"
            UPDATE users
            SET name = ($1)
            WHERE id = ($2)
            RETURNING *
            "#,
            value,
            user_id
        )
        .fetch_optional(&**pool)
        .await
        .map_err(|err| {
            tracing::error!("Failed to execute query: {:?}", err);
            err
        })
        .expect("failed to update user name");
    }
    Ok(HttpResponse::Ok().finish())
}

#[instrument(name = "Updating user data", skip(pool))]
pub async fn delete_user(
    claims: Claims,
    pool: web::Data<PgPool>,
) -> Result<HttpResponse, HttpResponse> {
    let user_id: Uuid = Uuid::parse_str(&claims.user_id).expect("failed to parse uuid");
    let _result = sqlx::query!(
        r#"
            DELETE FROM users
            WHERE id = ($1)
            "#,
        user_id
    )
    .fetch_optional(&**pool)
    .await
    .map_err(|err| {
        tracing::error!("Failed to execute query: {:?}", err);
        err
    })
    .expect("failed to delete user");

    let yesterday = chrono::Utc::now()
        .sub(chrono::Duration::days(1))
        .trunc_subsecs(0)
        .to_rfc2822();
    let cookie_name = "auth_token";

    Ok(HttpResponse::NoContent()
        .insert_header((
            "Set-Cookie",
            format!(
                "{}=\"\"; HttpOnly; SameSite=Strict; expires={}",
                cookie_name, yesterday
            ),
        ))
        .finish())
}

#[instrument(name = "Saving new user details to database", skip(user_data, pool))]
async fn insert_user(pool: &PgPool, user_data: &SignUpForm) -> Result<bool, sqlx::Error> {
    let result = sqlx::query!(
        r#"
        INSERT INTO users (id, email, name, password, registered_at)
        VALUES ($1, $2, $3, $4, $5)
        ON CONFLICT DO NOTHING
        RETURNING id
        "#,
        Uuid::new_v4(),
        user_data.email,
        user_data.name,
        user_data.password,
        Utc::now()
    )
    .fetch_optional(pool)
    .await
    .map_err(|err| {
        tracing::error!("Failed to execute query: {:?}", err);
        err
    })?;
    Ok(result.is_some())
}
