use crate::auth::{hash_password, Claims};
use actix_web::{web, HttpResponse};
use chrono::Utc;
use futures::TryStreamExt;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;
use sqlx::Row;
use tracing::instrument;
use uuid::Uuid;

#[derive(Deserialize)]
pub struct RegisterBoatForm {
    authentication_id: String,
    name: String,
    password: String,
}

#[derive(Deserialize, Debug, Serialize)]
struct BoatInfo {
    id: String,
    authentication_id: String,
    name: String,
    user_id: String,
}

#[instrument(name = "Fetching boats of session user", skip(pool))]
pub async fn list_boats(
    claims: Claims,
    pool: web::Data<PgPool>,
) -> Result<HttpResponse, HttpResponse> {
    let user_id: Uuid = Uuid::parse_str(&claims.user_id).expect("failed to parse uuid");
    let mut boats = Vec::with_capacity(3);
    let mut rows =
        sqlx::query("SELECT id, authentication_id, name, user_id FROM boats WHERE user_id = $1")
            .bind(&user_id)
            .fetch(&**pool);

    while let Some(row) = rows.try_next().await.expect("failed to read db row") {
        let id: Uuid = row.try_get("id").expect("failed to find column index id");
        let authentication_id: String = row
            .try_get("authentication_id")
            .expect("failed to find column index auth_id");
        let name: String = row
            .try_get("name")
            .expect("failed to find column index name");
        let user_id: Uuid = row
            .try_get("user_id")
            .expect("failed to find column index user_id");

        let boat_info = BoatInfo {
            id: id.to_string(),
            authentication_id,
            name,
            user_id: user_id.to_string(),
        };
        boats.push(boat_info);
    }

    Ok(HttpResponse::Ok().json(&boats))
}

#[instrument(name = "Adding a new boat", skip(boat_form, pool))]
pub async fn create_boat(
    claims: Claims,
    boat_form: web::Form<RegisterBoatForm>,
    pool: web::Data<PgPool>,
) -> Result<HttpResponse, HttpResponse> {
    let user_id: Uuid = Uuid::parse_str(&claims.user_id).expect("failed to parse uuid");
    if boat_form.authentication_id.is_empty()
        || boat_form.name.is_empty()
        || boat_form.password.is_empty()
    {
        return Ok(HttpResponse::BadRequest().finish());
    }

    let password = hash_password(&boat_form.password);
    let boat_data = RegisterBoatForm {
        password,
        ..boat_form.into_inner()
    };

    if insert_boat(&pool, &boat_data, &user_id)
        .await
        .map_err(|_| HttpResponse::InternalServerError().finish())?
    {
        Ok(HttpResponse::Created().finish())
    } else {
        Ok(HttpResponse::Conflict().finish())
    }
}

#[instrument(name = "Remove boat", skip(pool), fields(boat_id = %boat_id))]
pub async fn delete_boat(
    claims: Claims,
    boat_id: web::Path<Uuid>,
    pool: web::Data<PgPool>,
) -> Result<HttpResponse, HttpResponse> {
    let result = sqlx::query("DELETE FROM boats WHERE id = $1 RETURNING id")
        .bind(&boat_id.into_inner())
        .fetch_optional(&**pool)
        .await
        .map_err(|err| {
            tracing::error!("Failed to execute query: {:?}", err);
            err
        })
        .expect("failed to delete boat");
    if result.is_none() {
        return Ok(HttpResponse::NotFound().finish());
    }
    Ok(HttpResponse::NoContent().finish())
}

#[instrument(
    name = "Saving new boat details to database",
    skip(boat, user_id, pool)
)]
async fn insert_boat(
    pool: &PgPool,
    boat: &RegisterBoatForm,
    user_id: &Uuid,
) -> Result<bool, sqlx::Error> {
    let result = sqlx::query!(
        r#"
        INSERT INTO boats (id, authentication_id, name, password, user_id, created_at)
        VALUES ($1, $2, $3, $4, $5, $6)
        ON CONFLICT DO NOTHING
        RETURNING id
        "#,
        Uuid::new_v4(),
        boat.authentication_id,
        boat.name,
        boat.password,
        user_id,
        Utc::now()
    )
    .fetch_optional(pool)
    .await
    .map_err(|err| {
        tracing::error!("Failed to execute query: {:?}", err);
        err
    })?;
    Ok(result.is_some())
}
