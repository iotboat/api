use crate::routes::{
    create_boat, create_user, delete_boat, delete_user, end_session, health_check, list_boats,
    return_user_info, start_session, update_user, validate_session,
};
use actix_web::dev::Server;
use actix_web::web::Data;
use actix_web::{web, App, HttpServer};
use sqlx::PgPool;
use std::net::TcpListener;
use tracing_actix_web::TracingLogger;

pub fn run(listener: TcpListener, db_pool: PgPool) -> Result<Server, std::io::Error> {
    let db_pool = Data::new(db_pool);
    let server = HttpServer::new(move || {
        App::new()
            .wrap(TracingLogger)
            .service(
                web::scope("v1")
                    .route("/health_check", web::get().to(health_check))
                    .service(
                        web::scope("/boats")
                            .route("", web::get().to(list_boats))
                            .route("", web::post().to(create_boat))
                            .route("/{id}", web::delete().to(delete_boat)),
                    )
                    .service(
                        web::scope("/users")
                            .route("", web::get().to(return_user_info))
                            .route("", web::post().to(create_user))
                            .route("", web::put().to(update_user))
                            .route("", web::delete().to(delete_user)),
                    )
                    .service(
                        web::scope("/session")
                            .route("", web::get().to(validate_session))
                            .route("", web::post().to(start_session))
                            .route("", web::delete().to(end_session)),
                    ),
            )
            .app_data(db_pool.clone())
    })
    .listen(listener)?
    .run();
    Ok(server)
}
