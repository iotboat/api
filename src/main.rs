use iotboat_api::auth::create_secret;
use iotboat_api::configuration::get_configuration;
use iotboat_api::startup::run;
use iotboat_api::telemetry::{get_subscriber, init_subscriber};
use sqlx::PgPool;
use std::net::TcpListener;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let subscriber = get_subscriber("iotboat_api".into(), "info".into());
    init_subscriber(subscriber);
    std::env::set_var("JWT_SECRET", create_secret(16));
    let configuration = get_configuration().expect("Failed to read configuration.");
    let address = format!(
        "{}:{}",
        configuration.application.host, configuration.application.port
    );
    let listener = TcpListener::bind(address).expect("Failed to bind local address");
    let connection_string = configuration.database.connection_string();
    let pool = PgPool::connect(&connection_string)
        .await
        .expect("Failed to connect to database.");
    run(listener, pool)?.await
}
