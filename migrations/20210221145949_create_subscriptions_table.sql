CREATE TABLE users(
  id uuid PRIMARY KEY,
  email VARCHAR NOT NULL UNIQUE,
  name VARCHAR NOT NULL,
  password VARCHAR NOT NULL,
  registered_at timestamptz NOT NULL
);

CREATE TABLE boats(
  id uuid PRIMARY KEY,
  authentication_id VARCHAR NOT NULL UNIQUE,
  name VARCHAR NOT NULL,
  password VARCHAR NOT NULL,
  user_id uuid REFERENCES users(id) ON DELETE CASCADE,
  created_at timestamptz NOT NULL,
  UNIQUE (name, user_id)
);